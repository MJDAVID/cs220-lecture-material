# Project 9 (P9) grading rubric

## Code reviews

- A TA / grader will be reviewing your code after the deadline.
- They will make deductions based on the Rubric provided below.
- To ensure that you don’t lose any points in code review, you must review the rubric and make sure that you have followed the instructions provided in the project correctly.

## Rubric

### General guidelines:

- Displaying excessive irrelevant information or variables of large size (like `movies`) (-3)
- Required outputs not visible/did not save the notebook file prior to running the cell containing "export" (-3)
- Used concepts/modules (ex: `csv.DictReader`, `numpy`) not covered in class yet (built-in functions that you have been introduced to can be used). Deduction will also apply if `matplotlib` and `pandas` are used for any purpose other than plotting (-3)
- import statements are not mentioned in the required cell at the top of the notebook or used additional import statements beyond those that are stated in the directions (-1)
- Plot is not visible (-2 for each plot)

### Question specific guidelines:

- Q1 (3)
	- Required function is not used (-1)
	- Median calculation is incorrect (-1)
	- Answer uses loops to iterate over dictionary keys (-1)

- Q2 (4)
	- Required function is not used (-1)
	- Incorrect logic is used to answer (-1)
	- Answer uses loops to iterate over dictionary keys (-1)

- `bucketize` (5)
	- Function logic is incorrect (-3)
	- Categories are hardcoded while checking if they represent a list or not (-1)
	- Function is defined more than once (-1)

- `cast_buckets` (2)
	- Required function is not used (-1)
	- Data structure is defined incorrectly (-1)

- `director_buckets` (2)
	- Required function is not used (-1)
	- Data structure is defined incorrectly (-1)

- `genre_buckets` (2)
	- Required function is not used (-1)
	- Data structure is defined incorrectly (-1)

- `year_buckets` (2)
	- Required function is not used (-1)
	- Data structure is defined incorrectly (-1)

- Q3 (3)
	- Required data structure is not used (-2)

- Q4 (3)
	- Plot is not properly labeled (-1)
	- Plot is incorrect (-1)
	- Incorrect logic is used to answer (-1)

- Q5 (3)
	- Plot is not properly labeled (-1)
	- Plot is incorrect (-1)
	- Incorrect logic is used to answer (-1)

- Q6 (4)
	- Plot is not properly labeled (-1)
	- Plot is incorrect (-1)
	- Incorrect logic is used to answer (-1)

- Q7 (4)
	- Plot is not properly labeled (-1)
	- Plot is incorrect (-1)
	- Incorrect logic is used to answer (-1)

- Q8 (4)
	- Recomputed variable defined in Question 7 (-1)
	- Incorrect logic is used to answer (-2)

- Q9 (4)
	- Does not find only the movies directed by Martin Scorsese and starring Robert De Niro (-1)
	- Incorrect logic is used to answer (-2)

- Q10 (5)
	- Incorrect comparison operators are used (-1)
	- Incorrect logic is used to answer (-2)
